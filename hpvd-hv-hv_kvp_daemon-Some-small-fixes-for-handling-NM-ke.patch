From a8335c9675d22f8ed51a98e2f6b0f6f684d07793 Mon Sep 17 00:00:00 2001
From: Ani Sinha <anisinha@redhat.com>
Date: Mon, 16 Oct 2023 19:03:33 +0530
Subject: [PATCH 11/14] hv/hv_kvp_daemon: Some small fixes for handling NM
 keyfiles

RH-Author: Miroslav Rezanina <mrezanin@redhat.com>
RH-MergeRequest: 9: Synchronize RHEL 9 changes to RHEL 10
RH-Jira: RHEL-40107 RHEL-40679
RH-Acked-by: Ani Sinha <anisinha@redhat.com>
RH-Commit: [11/14] 55156e8fdd215ce1308887ea463a17ba614b1837 (mrezanin/centos-git-hyperv-daemons)

Some small fixes:
- lets make sure we are not adding ipv4 addresses in ipv6 section in
  keyfile and vice versa.
- ADDR_FAMILY_IPV6 is a bit in addr_family. Test that bit instead of
  checking the whole value of addr_family.
- Some trivial fixes in hv_set_ifconfig.sh.

These fixes are proposed after doing some internal testing at Red Hat.

Cherry-picked from upstream linux
kernel commit c3803203bc5ec910a ("hv/hv_kvp_daemon: Some small fixes for handling NM keyfiles")
CC: Shradha Gupta <shradhagupta@linux.microsoft.com>
CC: Saurabh Sengar <ssengar@linux.microsoft.com>
Fixes: 42999c904612 ("hv/hv_kvp_daemon:Support for keyfile based connection profile")
Signed-off-by: Ani Sinha <anisinha@redhat.com>
Reviewed-by: Shradha Gupta <Shradhagupta@linux.microsoft.com>
Signed-off-by: Wei Liu <wei.liu@kernel.org>
Message-ID: <20231016133122.2419537-1-anisinha@redhat.com>

patch_name: hpvd-hv-hv_kvp_daemon-Some-small-fixes-for-handling-NM-ke.patch
present_in_specfile: true
location_in_specfile: 14
---
 hv_kvp_daemon.c    | 20 ++++++++++++--------
 hv_set_ifconfig.sh |  4 ++--
 2 files changed, 14 insertions(+), 10 deletions(-)

diff --git a/hv_kvp_daemon.c b/hv_kvp_daemon.c
index 264eeb9..318e2da 100644
--- a/hv_kvp_daemon.c
+++ b/hv_kvp_daemon.c
@@ -1421,7 +1421,7 @@ static int kvp_set_ip_info(char *if_name, struct hv_kvp_ipaddr_value *new_val)
 	if (error)
 		goto setval_error;
 
-	if (new_val->addr_family == ADDR_FAMILY_IPV6) {
+	if (new_val->addr_family & ADDR_FAMILY_IPV6) {
 		error = fprintf(nmfile, "\n[ipv6]\n");
 		if (error < 0)
 			goto setval_error;
@@ -1455,14 +1455,18 @@ static int kvp_set_ip_info(char *if_name, struct hv_kvp_ipaddr_value *new_val)
 	if (error < 0)
 		goto setval_error;
 
-	error = fprintf(nmfile, "gateway=%s\n", (char *)new_val->gate_way);
-	if (error < 0)
-		goto setval_error;
-
-	error = fprintf(nmfile, "dns=%s\n", (char *)new_val->dns_addr);
-	if (error < 0)
-		goto setval_error;
+	/* we do not want ipv4 addresses in ipv6 section and vice versa */
+	if (is_ipv6 != is_ipv4((char *)new_val->gate_way)) {
+		error = fprintf(nmfile, "gateway=%s\n", (char *)new_val->gate_way);
+		if (error < 0)
+			goto setval_error;
+	}
 
+	if (is_ipv6 != is_ipv4((char *)new_val->dns_addr)) {
+		error = fprintf(nmfile, "dns=%s\n", (char *)new_val->dns_addr);
+		if (error < 0)
+			goto setval_error;
+	}
 	fclose(nmfile);
 	fclose(ifcfg_file);
 
diff --git a/hv_set_ifconfig.sh b/hv_set_ifconfig.sh
index 35aae6f..9c2ee30 100644
--- a/hv_set_ifconfig.sh
+++ b/hv_set_ifconfig.sh
@@ -53,7 +53,7 @@
 #                       or "manual" if no boot-time protocol should be used)
 #
 # address1=ipaddr1/plen
-# address=ipaddr2/plen
+# address2=ipaddr2/plen
 #
 # gateway=gateway1;gateway2
 #
@@ -61,7 +61,7 @@
 #
 # [ipv6]
 # address1=ipaddr1/plen
-# address2=ipaddr1/plen
+# address2=ipaddr2/plen
 #
 # gateway=gateway1;gateway2
 #
-- 
2.39.3

